<?php
    require_once 'connect.php';
    require_once 'users.php';
    require_once 'messages.php';

    if (isset($_POST["action"])){

        if($_POST["action"] == "get_all"){

            $start = get_messages();
            echo json_encode(['status' => true,'start'=>$start]);
        }
    }

    if (isset($_POST["action"])){

        if($_POST["action"] == "getLastId"){
            $attrLastId = $_POST['attr'];
            $selectedLastId = $conn->query("SELECT *, messages.id as MessageID FROM messages INNER JOIN users ON users.id = messages.userId WHERE  messages.id > $attrLastId");
            $lastID = mysqli_fetch_all($selectedLastId, MYSQLI_ASSOC);

            $userActivity = $conn->query ("SELECT * FROM users WHERE lastLogin >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)");
            $activeUsers = mysqli_fetch_all( $userActivity, MYSQLI_ASSOC);
            
            echo json_encode(['status' => true,'lastId'=>$lastID, 'activeUsers'=>$activeUsers]);
            
        }
    }

    if (isset($_POST["action"])){

        if($_POST["action"] == "add"){
            
            $message = $_POST["text"];
            $nickname = $_POST["name"];
            
        
            $data = $conn->query("SELECT name FROM users WHERE name ='$nickname'");
            $result = mysqli_fetch_all($data, MYSQLI_ASSOC);
            if(count($result) == 0 ) {
                add_user($nickname);
            } else {
                $conn->query("UPDATE users SET users.lastLogin = NOW() WHERE name='$nickname'");
            }
           
            
            $sql = mysqli_query($conn, "SELECT id FROM users WHERE name ='$nickname'");
            $array = mysqli_fetch_all($sql,MYSQLI_ASSOC);
            $UserId = $array[0]['id'];
            
            add_message($UserId, $message);
            $arr = get_messages();
            echo json_encode(['status' => true, 'arr' => $arr]);  
           
        }


    }
    
    
?>



