<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>chat</title>
    <link rel="stylesheet" href="style.css">
</head> 
<body>
    
    <div class="container">

        <div class="main">
            <div class="chat-window"></div>
            <div class="users-active"></div>
        </div>

        <div class="form">
            <input class="name" type="varchar" placeholder="nickname" pattern="\s">
            <textarea class="message-area" type="text" placeholder="message" ></textarea>
            <button class="send">Send</button>
        </div>

    </div>


    <script src="jquery.js"></script>
    <script src="scripts.js"></script>
</body>
</html>