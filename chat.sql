-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 13 2021 г., 10:24
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `chat`
--

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `message` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `userId`, `message`, `created`) VALUES
(495, 307, 'barev', '2021-05-12 17:39:29'),
(496, 308, 'barev', '2021-05-12 17:39:50'),
(497, 309, 'wow', '2021-05-12 19:04:43'),
(498, 308, 'dddd', '2021-05-12 19:31:33'),
(499, 309, 'rgrrg', '2021-05-12 19:33:16'),
(500, 309, 'regtrgt', '2021-05-12 19:36:37'),
(501, 309, 'dfg', '2021-05-12 19:39:26'),
(502, 309, 'rfgrg', '2021-05-12 19:41:47'),
(503, 309, 'rf', '2021-05-12 19:49:20'),
(504, 310, 'rgrggrfg', '2021-05-12 19:51:01'),
(505, 309, 'gfgdfg', '2021-05-12 19:59:21'),
(506, 309, 'grtgtr', '2021-05-12 20:02:06'),
(507, 309, 'ergrg', '2021-05-12 20:05:24'),
(508, 309, 'ggggg', '2021-05-12 20:09:15'),
(509, 309, 'thtyht', '2021-05-12 20:14:27'),
(510, 309, 'aaaaaaaaaaa', '2021-05-12 20:18:12'),
(511, 309, 'sefrfgdf', '2021-05-12 20:19:02'),
(512, 309, 'bbbb', '2021-05-12 20:20:50'),
(513, 309, 'ok', '2021-05-12 20:22:59'),
(514, 310, 'eeeey', '2021-05-12 20:23:29'),
(515, 311, 'asdqwewq', '2021-05-12 20:23:39'),
(516, 308, 'setrt', '2021-05-12 20:25:09'),
(517, 309, 'dfdf', '2021-05-12 20:58:30'),
(518, 308, 'dfdf', '2021-05-12 20:58:39'),
(519, 308, 'ergraegt', '2021-05-12 22:56:31'),
(520, 309, 'erf', '2021-05-12 23:03:39'),
(521, 309, 'sefr', '2021-05-12 23:31:03'),
(522, 308, 'defrf', '2021-05-12 23:37:49'),
(523, 308, 'barev', '2021-05-13 10:44:28'),
(524, 312, 'barev', '2021-05-13 10:45:40');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lastLogin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `lastLogin`) VALUES
(307, 'Narek', '2021-05-12 17:39:29'),
(308, 'Yura', '2021-05-13 10:44:28'),
(309, 'Garen', '2021-05-12 23:31:03'),
(310, 'Vram', '2021-05-12 20:23:29'),
(311, 'qxcx', '2021-05-12 20:23:39'),
(312, 'Karen', '2021-05-13 10:45:40');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=525;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=313;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
