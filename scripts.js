var nickname = document.querySelector(".name");
var messageArea = document.querySelector(".message-area");





function get_all() {
    $.ajax({
        url: "./chat.php",
        method: "post",
        dataType: "json",
        data: {
            action:"get_all",
        },
        success: function(messages){

            let myName = nickname.value.trim();
            
            messages.start.forEach(e => {

                if (e.name != myName) { 
                    $('.chat-window').append('<span class="message" data-id='+ e.MessageID +'>' + e.name + ' ' + ':' + ' ' + e.message + '</span>');
                }
                if (e.name == myName) {
                    $('.chat-window').append('<span class="myMessages" data-id='+ e.MessageID +'>' + myName + ' ' + ':'+ ' ' + e.message + '</span>')
                }
            });      
        }
                
    })    
}
get_all();

function getLastId() {
    
    let message = document.querySelectorAll(".message");
    let attr = message[message.length-1].getAttribute("data-id");
    let myName = nickname.value.trim();

    $.ajax({
        url: "./chat.php",
        method: "post",
        dataType: "json",
        data: {
            action:"getLastId",
            attr: attr,
        },
        success: function(messages){
            $(".users-active").html("");
            for ( j = 0; j < messages.lastId.length; j++) {
                
                if (messages.lastId[j].name != myName) { 
                    $('.chat-window').append('<span class="message" data-id=' + messages.lastId[j].MessageID + '>' + messages.lastId[j].name + ' ' + ':' + ' ' +messages.lastId[j].message + '</span>');
                }
                if (messages.lastId[j].name == myName) {
                    $('.chat-window').append('<span class="myMessages" data-id=' + messages.lastId[j].MessageID + '>' + myName + ' ' + ':'+ ' ' + messages.lastId[j].message + '</span>')
                }
            }    
            console.log(messages.activeUsers)
            messages.activeUsers.forEach(e => {
                
                $(".users-active").append('<span class="active" >' + e.name + '<span class="pointer"></span></span>');
                
            })
           
        }   
    })     
}


setInterval(function() {
    $(".pointer").css({"opacity": "0"});
    getLastId()
}, 2000);

document.querySelector(".send").addEventListener("click", send);

    function send() { 
        let myName = nickname.value.trim();
        
        if ( myName != "" && messageArea.value != "" ) {
            $.ajax({
                url: "./chat.php",
                method: "post",
                dataType: "json",
                data: {
                    name: myName,
                    text:  messageArea.value,
                    action:"add",
                },
                success: function(response){
                    console.log(response,'response')
                    if(response.status){
                        let all= response.arr;
                        for (let i = 0; i < all.length; i++) {
            
                        
                            var a = i;
                        
                        }

                        if (all[a].name == myName) {
                            $('.chat-window').append('<span class="myMessages message" data-id='+ all[a].MessageID +'>' + myName + ' ' + ':'+ ' ' + all[a].message + '</span>')
                        }
                        
                        if (all[a].name != myName) {
                            $('.chat-window').append('<span class="message" data-id='+ all[a].MessageID +'>' + all[a].name + ' ' + ':' + ' ' + all[a].message + '</span>')
                            
                        }
                    }
                    
                    
                },
                error: function(error) {
                    console.log(error)
                }
            })   
    }
    messageArea.value="";
    
}


